# Pirate Ships
In this application, I used Clean Architecture principles. The app is structured in 3 modules (layers): Domain, Data, and App.
The domain layer is an abstraction layer where I define the rules of my app. In the Data layer, I implement the abstraction of the Domain Layer.
In the App module, we have the UI layer - Activities, Fragments, etc...
The app is written in Kotlin and is structured with the MVVM pattern.
I used Koin for DI, Retrofit, Gson, Glide, RecyclerView, Navigation Components, etc...
The app is composed of 1 activity and 2 fragments.