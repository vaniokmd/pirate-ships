package com.varanita.data.mappers

import com.varanita.data.response.ShipResponse
import com.varanita.domain.entities.Ship

class ShipApiResponseMapper {
    fun toShipList(shipResponse: ShipResponse): List<Ship> {
        return shipResponse.ships.mapNotNull {
            it?.let {
                Ship(
                    it.id,
                    it.title,
                    it.description,
                    it.price.toString(),
                    it.image,
                    it.greeting_type
                )
            }

        }
    }
}