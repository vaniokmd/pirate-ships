package com.varanita.data.api

import com.varanita.data.response.ShipResponse
import retrofit2.Response
import retrofit2.http.GET

interface ShipsApi {
    @GET("mobile/interview-test/pirateships")
    suspend fun getShips(): ShipResponse
}