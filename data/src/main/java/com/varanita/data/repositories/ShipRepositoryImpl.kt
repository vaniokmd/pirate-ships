package com.varanita.data.repositories

import com.varanita.data.datasources.ShipRemoteDataSource
import com.varanita.domain.common.Result
import com.varanita.domain.entities.Ship
import com.varanita.domain.repositories.ShipRepository

class ShipRepositoryImpl(private val shipRemoteDataSource: ShipRemoteDataSource): ShipRepository {

    override suspend fun getShips(): Result<List<Ship>> {
        return shipRemoteDataSource.getShips()
    }
}