package com.varanita.data.response

import com.google.gson.annotations.SerializedName


data class ShipResponse (
    @SerializedName("success") val success : Boolean,
    @SerializedName("ships") val ships : List<ShipItem?>
)

data class ShipItem (
    @SerializedName("id") val id : Int,
    @SerializedName("title") val title : String?,
    @SerializedName("description") val description : String?,
    @SerializedName("price") val price : Int?,
    @SerializedName("image") val image : String?,
    @SerializedName("greeting_type") val greeting_type : String?
)