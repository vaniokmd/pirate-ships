package com.varanita.data.datasources

import com.varanita.data.api.ShipsApi
import com.varanita.data.mappers.ShipApiResponseMapper
import com.varanita.domain.common.Result
import com.varanita.domain.entities.Ship
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ShipRemoteDataSourceImpl(
    private val shipsApi: ShipsApi,
    private val shipApiResponseMapper: ShipApiResponseMapper
) : ShipRemoteDataSource {
    override suspend fun getShips(): Result<List<Ship>> =
        withContext(Dispatchers.IO) {
            try {
                val response = shipsApi.getShips()
                return@withContext Result.Success(shipApiResponseMapper.toShipList(response))
            } catch (exception: Exception) {
                return@withContext Result.Error(Exception(exception))
            }
        }
}