package com.varanita.data.datasources

import com.varanita.domain.common.Result
import com.varanita.domain.entities.Ship

interface ShipRemoteDataSource {
    suspend fun getShips(): Result<List<Ship>>
}