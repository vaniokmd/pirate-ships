package com.varanita.domain.usecases

import com.varanita.domain.repositories.ShipRepository

class GetShipsUseCase(private val shipRepository: ShipRepository) {
    suspend operator fun invoke() = shipRepository.getShips()
}