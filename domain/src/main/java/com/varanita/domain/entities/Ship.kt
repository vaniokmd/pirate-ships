package com.varanita.domain.entities

data class Ship (
    val id : Int,
    val title : String?,
    val description : String?,
    val price : String?,
    val image : String?,
    val greeting_type : String?
)