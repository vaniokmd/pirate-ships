package com.varanita.domain.repositories

import com.varanita.domain.common.Result
import com.varanita.domain.entities.Ship

interface ShipRepository {
    suspend fun getShips(): Result<List<Ship>>
}