package com.varanita.pyrateships.extensions

import android.widget.ProgressBar
import androidx.core.view.isVisible

import androidx.databinding.BindingAdapter

@BindingAdapter(value = ["setupVisibility"])
fun ProgressBar.progressVisibility(isLoading: Boolean) {
    this.isVisible = isLoading
}