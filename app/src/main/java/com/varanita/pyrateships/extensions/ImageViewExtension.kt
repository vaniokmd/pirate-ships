package com.varanita.pyrateships.extensions

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.varanita.pyrateships.R


@BindingAdapter(value = ["setImageUrl"])
fun ImageView.bindImageUrl(url: String?) {
    url?.let {
        if (it.isNotBlank()) {
            Glide.with(context).load(it).placeholder(R.mipmap.ic_launcher_round).dontAnimate()
                .into(this)
        }
    }

}