package com.varanita.pyrateships.modules

import com.varanita.data.api.ShipsApi
import com.varanita.data.datasources.ShipRemoteDataSource
import com.varanita.data.mappers.ShipApiResponseMapper
import com.varanita.data.datasources.ShipRemoteDataSourceImpl
import com.varanita.data.repositories.ShipRepositoryImpl
import com.varanita.domain.repositories.ShipRepository
import com.varanita.domain.usecases.GetShipsUseCase
import com.varanita.pyrateships.ui.MainViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    single { provideShipRemoteDataSource(get()) }
    single { provideShipRepository(get()) }
    single { provideGetShipsUseCase(get()) }
    viewModel { MainViewModel(get()) }
}


private fun provideShipRemoteDataSource(shipsApi: ShipsApi): ShipRemoteDataSource {
    return ShipRemoteDataSourceImpl(
        shipsApi,
        ShipApiResponseMapper()
    )
}

private fun provideShipRepository(shipRemoteDataSource: ShipRemoteDataSource): ShipRepository {
    return ShipRepositoryImpl(shipRemoteDataSource)
}

private fun provideGetShipsUseCase(shipRepository: ShipRepository): GetShipsUseCase {
    return GetShipsUseCase(shipRepository)
}
