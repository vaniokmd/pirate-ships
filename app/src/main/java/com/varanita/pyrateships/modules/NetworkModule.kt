package com.varanita.pyrateships.modules

import com.varanita.data.*
import com.varanita.data.api.ShipsApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    factory { provideOkHttpInterceptor() }
    factory { provideOkHttpClient(get()) }
    factory { provideRetrofit(get()) }
    single { provideApi(get()) }
}
private fun provideOkHttpInterceptor(): HttpLoggingInterceptor {
    val logginInterceptor = HttpLoggingInterceptor()
    logginInterceptor.level = HttpLoggingInterceptor.Level.BODY
    return logginInterceptor
}

private fun provideOkHttpClient(loggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
    return OkHttpClient().newBuilder().addInterceptor(loggingInterceptor).build()
}

private fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder().baseUrl(BuildConfig.API_URL).client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create()).build()
}

private fun provideApi(retrofit: Retrofit): ShipsApi {
    return retrofit.create(ShipsApi::class.java)
}


