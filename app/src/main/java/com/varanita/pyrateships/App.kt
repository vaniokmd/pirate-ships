package com.varanita.pyrateships

import android.app.Application
import com.varanita.pyrateships.modules.networkModule
import com.varanita.pyrateships.modules.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(listOf(networkModule, viewModelModule))
        }
    }
}