package com.varanita.pyrateships.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.varanita.data.response.ShipItem
import com.varanita.domain.common.Result
import com.varanita.domain.entities.Ship
import com.varanita.domain.repositories.ShipRepository
import com.varanita.domain.usecases.GetShipsUseCase
import kotlinx.coroutines.launch

class MainViewModel(private val getShipsUseCase: GetShipsUseCase): ViewModel() {

    private val _observableShips = MutableLiveData<List<Ship>>()
    val observableShips: LiveData<List<Ship>> = _observableShips

    private val _dataLoading = MutableLiveData(true)
    val dataLoading: LiveData<Boolean> = _dataLoading

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    val selectedShip = MutableLiveData<Ship>()


    init {
        fetchShips()
    }

    fun fetchShips() {
        viewModelScope.launch {
            _dataLoading.postValue(true)
            when(val ships = getShipsUseCase.invoke()) {
                is Result.Success -> {
                    _observableShips.postValue(ships.data)
                    _dataLoading.postValue(false)
                }
                is Result.Error -> {
                    _dataLoading.postValue(false)
                    _error.postValue(ships.exception.message)
                }
            }
        }
    }
}