package com.varanita.pyrateships.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.varanita.pyrateships.R
import com.varanita.pyrateships.databinding.FragmentShipDetailBinding
import com.varanita.pyrateships.ui.MainViewModel
import org.koin.android.viewmodel.ext.android.sharedViewModel

class ShipDetailFragment : Fragment() {

    private val viewModel by sharedViewModel<MainViewModel>()
    private lateinit var binding: FragmentShipDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(layoutInflater, R.layout.fragment_ship_detail, container, false)
        binding.apply {
            lifecycleOwner = this@ShipDetailFragment
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.selectedShip.observe(viewLifecycleOwner, Observer {
            binding.ship = it
            binding.executePendingBindings()
        })
        viewModel.selectedShip.value?.greeting_type?.let { greeting ->
            binding.btGreeding.visibility = View.VISIBLE
            binding.btGreeding.setOnClickListener {
                val builder = AlertDialog.Builder(requireContext())
                with(builder) {
                    setTitle(greeting)
                    setPositiveButton(android.R.string.ok) { dialog, _ ->
                        dialog.dismiss()
                    }
                }
                builder.show()
            }
        }

    }

}