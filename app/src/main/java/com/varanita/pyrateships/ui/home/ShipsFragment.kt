package com.varanita.pyrateships.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import com.varanita.pyrateships.R
import com.varanita.pyrateships.databinding.FragmentShipsBinding
import com.varanita.pyrateships.ui.MainViewModel
import kotlinx.coroutines.selects.select
import org.koin.android.viewmodel.ext.android.sharedViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class ShipsFragment : Fragment() {
    private val viewModel by sharedViewModel<MainViewModel>()
    private lateinit var binding: FragmentShipsBinding
    private val shipAdapter by lazy { ShipAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_ships, container, false)
        binding.apply {
            lifecycleOwner = this@ShipsFragment
            viewModel = this@ShipsFragment.viewModel

            adapter = shipAdapter
            rvShips.addItemDecoration(
                DividerItemDecoration(
                    rvShips.context,
                    DividerItemDecoration.VERTICAL
                )
            )
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.observableShips.observe(viewLifecycleOwner, Observer {
            binding.adapter?.submitList(it)
        })

        shipAdapter.onShipClick = {
            viewModel.selectedShip.postValue(it)
            findNavController().navigate(R.id.action_shipsFragment_to_shipDetailFragment)
        }


    }

}