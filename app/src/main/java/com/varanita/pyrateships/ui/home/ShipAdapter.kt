package com.varanita.pyrateships.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.varanita.domain.entities.Ship
import com.varanita.pyrateships.databinding.ShipItemBinding

class ShipAdapter : ListAdapter<Ship, ShipAdapter.ShipViewHolder>(
    Companion
) {

    var onShipClick: ((Ship) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShipViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding =
            ShipItemBinding.inflate(
                layoutInflater
            )
        val lp = RecyclerView.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        binding.root.layoutParams = lp
        return ShipViewHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: ShipViewHolder, position: Int) {
        val ship = getItem(position)
        holder.shipItemBinding.ship = ship
        holder.shipItemBinding.executePendingBindings()
    }

    inner class ShipViewHolder(val shipItemBinding: ShipItemBinding) :
        RecyclerView.ViewHolder(shipItemBinding.root) {
        init {
            shipItemBinding.root.setOnClickListener {
                onShipClick?.invoke(getItem(adapterPosition))
            }
        }
    }

    companion object : DiffUtil.ItemCallback<Ship>() {
        override fun areItemsTheSame(oldItem: Ship, newItem: Ship): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Ship, newItem: Ship): Boolean {
            return oldItem.id == newItem.id
        }

    }

}