package com.varanita.pyrateships.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.varanita.data.api.ShipsApi
import com.varanita.data.datasources.ShipRemoteDataSourceImpl
import com.varanita.data.mappers.ShipApiResponseMapper
import com.varanita.data.repositories.ShipRepositoryImpl
import com.varanita.data.response.ShipItem
import com.varanita.data.response.ShipResponse
import com.varanita.domain.entities.Ship
import com.varanita.domain.usecases.GetShipsUseCase
import kotlinx.coroutines.*
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException


@RunWith(JUnit4::class)
class MainViewModelTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @ObsoleteCoroutinesApi
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    private lateinit var viewModel: MainViewModel

    private val shipApiResponseMapper by lazy { ShipApiResponseMapper() }

    @Mock
    private lateinit var shipsApi: ShipsApi

    @Mock
    private lateinit var observer: Observer<List<Ship>>


    @ObsoleteCoroutinesApi
    @ExperimentalCoroutinesApi
    @Before
    fun setup() {
        Dispatchers.setMain(mainThreadSurrogate)
        MockitoAnnotations.initMocks(this)
        val getShipsUseCase = GetShipsUseCase(
            ShipRepositoryImpl(
                ShipRemoteDataSourceImpl(
                    shipsApi,
                    shipApiResponseMapper
                )
            )
        )
        viewModel = MainViewModel(getShipsUseCase)
        viewModel.observableShips.observeForever(observer)


    }

    @Test
    fun testNull() {
        runBlocking {
            launch(Dispatchers.Main) {
                `when`(shipsApi.getShips()).thenReturn(null)
                assertNotNull(viewModel.observableShips)
                assertTrue(viewModel.observableShips.hasObservers())
            }
        }

    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

}